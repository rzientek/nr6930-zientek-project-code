# NR6930-Zientek-Project-Code

## Zonal Statistics as Table to Excel
### This code will create a zonal statisitics table then export to Excel
#### It will be best to put the feature or raster file that defines the zone data into a separate folder and the rasters from which you want to create zonal statistics in another folder.
#### The variable folder for which you will set the workspace will contain the rasters from which statistics will be calculated.¶
#### The Excel files will be created and stored in the folder that hold the file that defines the zone data.
#### The output Excel file will have the name of the zone data file and the raster file combined.